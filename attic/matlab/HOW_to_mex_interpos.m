%
% Series of examples to mex interpos.f90 on various machines (and over time since evolves with both matlab versions and compilers installed)
%
% Tried to make it such that name of the machines works right away, but might need to find a new way, thus get inspired by other trials listed here..
%

machine = getenv('HOSTNAME')

if strcmp(lower(machine(1:3)),'lac') & strcmp(lower(machine(end-2:end)),'.ch')
  % on lacs at epfl.ch (ifort not yet working, to check)
  mex FC=gfortran interpos.f90 % note ifort not working June 2015)

elseif strcmp(lower(machine(end-7:end)),'iter.org')
  % on hpc4 ITER-IO
  mex FC=gfortran interpos.f90 % note ifort not working June 2015)
  % adding -v gave these commands (that can be executed in shell level) (June 2015):
  % gfortran -c  -I/opt/matlab/2012b/extern/include -I/opt/matlab/2012b/simulink/include -fexceptions -fbackslash -fPIC -fno-omit-frame-pointer  -DMX_COMPAT_32 -O  "interpos.f90"
  % gfortran -O -pthread -shared -Wl,--version-script,/opt/matlab/2012b/extern/lib/glnxa64/fexport.map -Wl,--no-undefined -o  "interpos.mexa64"  interpos.o  -Wl,-rpath-link,/opt/matlab/2012b/bin/glnxa64 -L/opt/matlab/2012b/bin/glnxa64 -lmx -lmex -lmat -lm

elseif strcmp(lower(machine(1:2)),'g0') & strcmp(lower(machine(end-2:end)),'.de')
  % gateway Garching
  mex FC=gfortran interpos.f90 
  % mex FC=ifort interpos.f90 (also worked on June 2015 yielding (removing -fbackslash and -pthread not used):
  % ifort -c  -I/afs/ipp-garching.mpg.de/common/soft/matlab_2013b/extern/include -I/afs/ipp-garching.mpg.de/common/soft/matlab_2013b/simulink/include -fexceptions -fPIC -fno-omit-frame-pointer  -DMX_COMPAT_32 -O  "interpos.f90"
  % ifort -O  -shared -Wl,--version-script,/afs/ipp-garching.mpg.de/common/soft/matlab_2013b/extern/lib/glnxa64/fexport.map -Wl,--no-undefined -o  "interpos.mexa64"  interpos.o  -Wl,-rpath-link,/afs/ipp-garching.mpg.de/common/soft/matlab_2013b/bin/glnxa64 -L/afs/ipp-garching.mpg.de/common/soft/matlab_2013b/bin/glnxa64 -lmx -lmex -lmat -lm


elseif strcmp(lower(machine(1:3)),'tok') & strcmp(lower(machine(end-2:end)),'.de')
  % toks machines at IPP-Garching
  mex FC=gfortran interpos.f90 
  
end

interpostest_time

disp(' ')
disp('successful if 4 "Elapsed time" appeared')
disp(' ')
