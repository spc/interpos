dnl  ====================================================
dnl             MATLAB library macros
dnl  ====================================================

dnl
dnl  AX_MATLAB - find Matlab libraries
dnl  ----------------------------------------------------
AC_DEFUN([AX_MATLAB],
[
AC_REQUIRE([AC_CANONICAL_HOST])
AC_PATH_PROG([BASENAME], [basename])
AC_PATH_PROG([DIRNAME], [dirname])
AC_PATH_PROG([TAIL], [tail])
AC_PATH_PROG([SED], [sed])

dnl Make MATLAB and precious variables.
AC_ARG_VAR([MATLAB_DIR], [The matlab directory, e.g. /opt/MATLAB/2018a])
AC_ARG_VAR([MATLAB], [The matlab executable, e.g. $MATLAB_DIR/bin/matlab])
AC_ARG_VAR([MEX], [The mex executable, e.g. $MATLAB_DIR/bin/mex])
dnl Also allow substition in Makefiles
AC_SUBST([MEX])

AC_ARG_WITH([matlab],
  [AS_HELP_STRING([--with-matlab],
    [support for MATLAB (optionally specify MATLAB directory)])],
  [],
  [with_matlab=check]
)

dnl Skip matlab check if 'no'
dnl Do checks if 'check' or 'yes' or '/some/path/to/matlab'
dnl Fail if 'yes' or '/some/path' and check failed.
AS_IF([test "x$with_matlab" = "xno"],
  [ AC_MSG_NOTICE([checking for MATLAB skipped: --with-matlab=no specified]) ],
  [ dnl Do matlab checks
    dnl If --with-matlab=/path/to/matlab dir given, 
    dnl set or override MATLAB variable
    AS_CASE([$with_matlab],
      [yes],[],
      [check],[],
      [ dnl User may specify path to matlab directory
	AS_IF([! test -d "$with_matlab"], [AC_MSG_ERROR([--with-matlab=DIR specified, but argument is not a directory: $with_matlab])])
        MATLAB_DIR="$with_matlab"
        with_matlab="yes"
    ])
])

AS_IF([test "x$with_matlab" != "xno"],[
  dnl If MATLAB points to a funny executable, test only that,
  dnl instead of the default
  AS_IF([test "x$MATLAB" = "x"],
    [ _AX_MATLAB_BIN_LIST="matlab matlab900 matlab850" ],
    [ _AX_MATLAB_BIN_LIST="`$BASENAME $MATLAB`"
      MATLAB_DIR="`$DIRNAME $MATLAB`"
      dnl AC_MSG_NOTICE([with_matlab: $with_matlab MATLAB_DIR: $MATLAB_DIR _AX_MATLAB_BIN_LIST: $_AX_MATLAB_BIN_LIST])
  ])
  dnl Test and set MATLAB to full path
  AS_IF([test "x$MATLAB_DIR" = "x"],
    [ AC_PATH_PROGS([MATLAB], [$_AX_MATLAB_BIN_LIST],[""], [$PATH]) ],
    [ AC_PATH_PROGS([MATLAB], [$_AX_MATLAB_BIN_LIST],[""], [$MATLAB_DIR/bin:$MATLAB_DIR]) ]
  )
])

AS_IF([test "x$with_matlab" != "xno"],[
  dnl If MEX points to a funny executable, test only that,
  dnl instead of the default
  AS_IF([test "x$MEX" = "x"],
    [ _AX_MEX_BIN_LIST="mex mex900 mex850" ],
    [ _AX_MEX_BIN_LIST="`$BASENAME $MEX`"
      MEX_DIR="`$DIRNAME $MEX`"
  ])
  dnl If MEX or MEX_DIR was not given, but MATLAB_DIR was, use it.
  dnl This ensures the mex command from the same dir as matlab is found.
  AS_IF([test "x$MEX_DIR" = "x" -a "x$MATLAB_DIR" != "x"],[MEXDIR="$MATLAB_DIR"])
  dnl Test and set MEX to full path
  AS_IF([test "x$MEX_DIR" = "x"],
    [ AC_PATH_PROGS([MEX], [$_AX_MEX_BIN_LIST],[""], [$PATH]) ],
    [ AC_PATH_PROGS([MEX], [$_AX_MEX_BIN_LIST],[""], [$MEX_DIR/bin:$MEX_DIR]) ]
  )
])

dnl If not found, fail only if --with-matlab is set
AS_IF([test "x$with_matlab" = "xyes"],[
  AS_IF([test "x$MATLAB" = "x"],[
    AC_MSG_FAILURE([--with-matlab specified, but no matlab executable found])
  ],[
    dnl Do a version check by running matlab.
    AC_CACHE_CHECK([for $MATLAB version],[ax_cv_matlab_version],
      [ ax_cv_matlab_version="`$MATLAB -nojvm -nosplash -r 'disp(version);quit' | $TAIL -n1`" 
    ])
    AS_IF([test "x$ax_cv_matlab_version" = "x"],[AC_MSG_FAILURE([Could not run $MATLAB to get version.])])
    have_matlab="yes" 
  ])
])

AM_CONDITIONAL(HAVE_MATLAB, [test "x$have_matlab" = "xyes"] )

AS_IF([test "x$have_matlab" = "xyes"],
[ dnl Add Matlab variables
  dnl depending on OS
  
  AS_CASE([$build_os],
    [linux|linux-gnu|*linux*],[
      MATLAB_FLAGS="-I${MATLAB_DIR}/extern/include -I${MATLAB_DIR}/simulink/include -DMATLAB_MEX_FILE -fPIC -ansi -D_GNU_SOURCE -pthread -O -DNDEBUG"
      MATLAB_LDFLAGS="-pthread -shared -W1,--version-script,${MATLAB_DIR}/extern/lib/glnxa86/fexport.map"
      MATLAB_LIBS="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/glnxa86,--rpath-link,${MATLAB_DIR}/bin/glnxa86 -L${MATLAB_DIR}/bin/glnxa86 -lmx -lmex -lmat -lm -mwlapack"
      MEXEXT=mexa64
      dnl What about -fexceptions -fno-omit-frame-pointer -DMX_COMPAT_32 ?
      dnl TODO: replace hard coded LIBS with AC_CHECK_LIB() tests
      dnl TODO: test what extension is produced.
    ],
    dnl TODO: Other builds? see below for some hints
    [AC_MSG_FAILURE([Unknown build_os: $build_os])]
  )

dnl  case $build_os in
dnl    *linux*)
dnl        MATLAB_FLAGS="-I${MATLAB_DIR}/extern/include -I${MATLAB_DIR}/simulink/include -DMATLAB_MEX_FILE -fPIC -ansi -D_GNU_SOURCE -pthread -O -DNDEBUG";
dnl        MATLAB_LINK="-pthread -shared -W1,--version-script,${MATLAB_DIR}/extern/lib/glnxa86/mexFunction.map";
dnl        MATLAB_LIB="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/glnxa86,--rpath-link,${MATLAB_DIR}/bin/glnxa86 -L${MATLAB_DIR}/bin/glnxa86 -lmx -lmex -lmat -lm";
dnl        MEXEXT=mexglx;;
dnl    *cygwin*)
dnl        MATLAB_FLAGS="-I${MATLAB_DIR}/extern/include -I${MATLAB_DIR}/simulink/include -fno-exceptions -mno-cygwin -DMATLAB_MEX_FILE -DNDEBUG";
dnl        MATLAB_LINK="-shared -mno-cygwin -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
dnl        MATLAB_LIB="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/win32,--rpath-link,${MATLAB_DIR}/bin/win32 ${MATLAB_DIR}/bin/win32/libmx.a ${MATLAB_DIR}/bin/win32/libmex.a ${MATLAB_DIR}/bin/win32/libmat.a -lm";
dnl        MATLAB_LINK="-shared -mno-cygwin -L${MATLAB_DIR}/bin/win32 -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
dnl        MATLAB_LIB="-lmx -lmex -lmat -lm";
dnl        MEXEXT=dll;
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmx.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmx=`dlltool -llibmx.a -d${MATLAB_DIR}/extern/include/libmx.def -Dlibmx.dll`
dnl            cd -
dnl        fi
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmex=`dlltool -llibmex.a -d${MATLAB_DIR}/extern/include/libmex.def -Dlibmex.dll`
dnl            cd -
dnl        fi
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmat=`dlltool -llibmat.a -d${MATLAB_DIR}/extern/include/libmat.def -Dlibmat.dll`
dnl            cd -
dnl        fi;;
dnl    *mingw*)
dnl        MATLAB_FLAGS="-I${MATLAB_DIR}/extern/include -I${MATLAB_DIR}/simulink/include -fno-exceptions -DMATLAB_MEX_FILE -DNDEBUG";
dnl        MATLAB_LINK="-shared -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
dnl        MATLAB_LIB="-W1,--rpath-link,${MATLAB_DIR}/extern/lib/win32,--rpath-link,${MATLAB_DIR}/bin/win32 ${MATLAB_DIR}/bin/win32/libmx.a ${MATLAB_DIR}/bin/win32/libmex.a ${MATLAB_DIR}/bin/win32/libmat.a -lm";
dnl        MATLAB_LINK="-shared -L${MATLAB_DIR}/bin/win32 -W1,--version-script,${MATLAB_DIR}/extern/lib/win32/mexFunction.def";
dnl        MATLAB_LIB="-lmx -lmex -lmat -lm";
dnl        MEXEXT=dll;
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmx.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmx=`dlltool -llibmx.a -d${MATLAB_DIR}/extern/include/libmx.def -Dlibmx.dll`
dnl            cd -
dnl        fi
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmex=`dlltool -llibmex.a -d${MATLAB_DIR}/extern/include/libmex.def -Dlibmex.dll`
dnl            cd -
dnl        fi
dnl        if test ! -e "${MATLAB_DIR}/bin/win32/libmex.a"
dnl        then
dnl            cd ${MATLAB_DIR}/bin/win32
dnl            libmat=`dlltool -llibmat.a -d${MATLAB_DIR}/extern/include/libmat.def -Dlibmat.dll`
dnl            cd -
dnl        fi;;
dnl    esac
dnl    AC_MSG_RESULT($MATLAB_LINK $MATLAB_LIB)
])

dnl
AC_SUBST(MATLAB_DIR)
AC_SUBST(MATLAB_LIBS)
AC_SUBST(MATLAB_LDFLAGS)
AC_SUBST(MATLAB_CFLAGS)
AC_SUBST(MEXEXT)

])
