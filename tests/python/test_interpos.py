
import numpy as np
import matplotlib.pyplot as plt
from py_interpos import interpos

x = np.linspace(0,6*np.pi,50000)
y = np.sin(x)**2
xspl2 = np.linspace(-3*np.pi,9*np.pi,3000000)
taus = 1e-6

xper = x[:-1]
yper = y[:-1]
nbcper = np.array([-1])
ybcper = np.array([6*np.pi])
sigmaper = np.ones_like(xper)

youtper = interpos(13,xper,yper,xspl2,taus,nbcper,ybcper,sigmaper)

nbc = np.array([2,2])
ybc = np.array([0,0])
sigma = np.ones_like(x)

yout = interpos(13,x,y,xspl2,taus,nbc,ybc,sigma)

plt.ion()

plt.plot(x[::100],y[::100],'-',xspl2[::10000],yout[0][::10000],'+',xspl2[5000::10000],youtper[0][5000::10000],'o')
plt.gca().set_ylim([-0.5,2.5])
