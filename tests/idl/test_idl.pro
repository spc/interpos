;--------------------------------------------------------------------
function test_idl, ier,                       $ ;;; --> error code from fit routine
                        x_in                       ;;; <-- independent values of profile (x-axis)

 ;; definition of variables
 ioptout = long(ier)
 xin=double([0., 0.1, 0.2, 0.3, 1.0])
 yin=2*xin^4+1.53
 xin=double([0., 0.1, 0.2, 0.3, 0.7, 1.0])
 yin=double([1.5, 1.3, 1, 0.6, 0.2, 0.05])
 xout=xin
 xout=double([0, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.9, 0.95, 1.])
 tension = double(-1.23)
 npoints = long(n_elements(xin))
 yout=DblArr(fix(npoints))
 nout = long(n_elements(xout))
 nbc = long([1 ,0])
 ybc = double([0.123, 0.2])
 sigma = xin+1
 option = long(63)
 info = long(0)
 yout=DblArr(fix(nout))
 youtp=yout
 youtpp=yout
 youtint=yout
 ;; call external fitting routine
 s=0
; s = call_external('/afs/ipp-garching.mpg.de/home/o/osauter/interpos_develop/interpos_libs/gfortran_idl/libinterposgfortran_idl.so', $
;                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, nout, nbc, ybc,sigma)
;                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, nout)
;                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, npoints, nbc, ybc,sigma,option,info, youtp,youtpp,youtint)
 print, s
 print, ioptout
 print, yout

 npoints = long(11)
 t=FINDGEN(fix(npoints))/(npoints-1)*2*3.141592653589793
 xin=double(t)
 yin=double(sin(xin)+0.0*randomu(seed,fix(npoints)))
 xout=double((FINDGEN(21)/20*4.-1.)*3.141592653589793)
 nout=long(n_elements(xout))
 yout=DblArr(fix(nout))
 youtp=DblArr(fix(nout))
 youtp=double(0*xout)
 youtpp=DblArr(fix(nout))
 tension= double(-1.)
 ioptout = 2L
 nbc=long([0, 0])
 ybc=double([0., 0.])
 print,youtp
 yout[1]=double(1.0)
 s = call_external('/afs/ipp-garching.mpg.de/home/o/osauter/interpos_develop/interpos_libs/gfortran_idl/libinterposgfortran_idl.so', $
                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, nout, youtp )
 print, s
 plot,xout,yout
 oplot,xin,yin
 oplot,xout,youtp
 nbc=long(-1)
 ybc=double(2*3.141592653589793)
 tension = double(-1.0)
 ioptout = 2L
 s = call_external('/afs/ipp-garching.mpg.de/home/o/osauter/interpos_develop/interpos_libs/gfortran_idl/libinterposgfortran_idl.so', $
                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, nout, nbc, ybc,youtp)
 iplot,xout,yout
 oplot,xin, yin
 oplot,xout,youtp
 return, yout
 xyout=DINDGEN(fix(nout),2)
 xyout[*,0]=xout
 xyout[*,1]=yout

 xout=double([1.23, 2.45])
 nout=long(n_elements(xout))
 yout=DblArr(nout)
 s = call_external('/afs/ipp-garching.mpg.de/home/o/osauter/interpos_develop/interpos_libs/gfortran_idl/libinterposgfortran_idl.so', $
                   'interpos_wrapper_c',ioptout, xin, yin, npoints, yout, tension, xout, nout, nbc, ybc)
 print,yout
 return, yout
end
