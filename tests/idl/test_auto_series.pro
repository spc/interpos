function test_auto_series
;
; call all cases of test_idl_interpos_all_cases with various values for tension and options
;

nb_cases = 6
FOR i=1, nb_cases DO BEGIN
   print,'case = ',i
   aa=test_idl_interpos_all_cases(i)
   wait, 2
ENDFOR

tension=-3.
FOR i=1, nb_cases DO BEGIN
   print,'case = ',i
   aa=test_idl_interpos_all_cases(i,tension)
   wait, 2
ENDFOR

; end with example with 0 for extrapolation
a=test_idl_interpos_all_cases(4,-10.,-63)

return, nb_cases
end
