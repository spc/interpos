function [varargout] = interpos_nan(var1, var2, var3, varargin);
%
% see help interpos for details
%
% pre-exclude non-finite values from either xin or yin before calling interpos
%
nverbose = 1; % warnings
%
if nargin < 3
  error('xin and yin required');
else
  if isscalar(var1)
    xin = var2;
    yin = var3;
    kopt_provided = 1;
  else
    xin = var1;
    yin = var2;
    kopt_provided = 0;
  end
  if nverbose >= 1 && (any(~isfinite(xin)) || any(~isfinite(yin))), warning('NaNs/Infs removed by interpos_nan'); end
end

ij = find(isfinite(xin) & isfinite(yin));
if ~isempty(ij)
  xin_eff = xin(ij);
  yin_eff = yin(ij);
  if kopt_provided == 1
    [varargout{1:nargout}] = interpos(var1, xin_eff, yin_eff, varargin{:});
  else
    [varargout{1:nargout}] = interpos(xin_eff, yin_eff, var3, varargin{:});
  end
else
  if nargout > 0
    varargout = cell(nargout,1);
    warning('Only NaNs in xin/yin');
  end
end
