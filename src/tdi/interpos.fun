fun public interpos(in _xin,in _yin,optional in _tension,optional in _xout)
/*
use: interpos(in _xin,in _yin,optional in _tension,optional in _xout)
     call to cubic spline with tension developed by O. Sauter using one extra input for the idl/C interface:
     if _xout not defined, return fit values on _xin grid
     if _xout and _tension defined, return on _xout grid
     ==================================================================================================================
     interpos_wrapper_c(noptout,xin,yin,nin,yout[,tension,xout,nout,nbc,ybc,sigma,option,info[,youtp,youtpp,youtint]]])
     interpos_wrapper_c(noptout,xin,yin,nin,yout[,p0     ,p1  ,p2  ,p3 ,p4 ,p5   ,p6    ,p7  [,p8   ,p9    ,p10    ]]])
c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff,nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff,youtp,youtpp,youtint);
c_wrapper_to_interpos(nbarg ,noptout,xin,yin,nin,yout,p0       ,p1         ,p2      ,p3     ,p4     ,p5       ,p6        ,p7      ,p8   ,p9    ,p10)
     ==================================================================================================================
     . the defaults will be decided within the f90 world
     
     Thus typical calls can be:
     interpos_wrapper_c(1,xin,yin,nin,yout); (to compute just the interpolation on xout=xin and tension=0, standard cubic spline)
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout); (to compute just the interpolation on xout)
     (see http://spc.epfl.ch/interpos for more details)

        Interface: BPDuval: [SPC, EPFL, Switzerland]      March 2016
*/
{
  _ioptout=1l;		/* Default interpos mode for this routine */
  _dummy_double = -9.d9;
  _dummy_long   = -99l;
/* if _xout defined, use _tension too */
  if(present(_xout)) {
   _nout=size(_xout)+0l;
   _nbarg=7l;
  } else {
   _nout=size(_xin)+0l;
   _tension=_dummy_double;
   _xout=_dummy_double;
   _nbarg=5l;
  }
  _nin=size(_xin)+0l;
  _yout=(1.._nout)+0d0;
  return(c_wrapper_to_interpos(_nbarg,_ioptout,_xin,_yin,_nin,_yout,_nout,_tension,_xout));
}
