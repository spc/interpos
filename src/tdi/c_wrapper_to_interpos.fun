fun public c_wrapper_to_interpos(in _nbarg,in _noptout,in _xin,in _yin,in _nin,inout _yout,optional in _p0,optional in _p1,optional in _p2,optional in _p3,optional in _p4,optional in _p5,optional in _p6,optional in _p7,optional inout _p8,optional inout _p9,optional inout _p10)
/*
use: c_wrapper_to_interpos(in _nbarg,in _noptout,in _xin,in _yin,in _nin,in _yout,optional in _p0,optional in _p1,optional in _p2,optional in _p3,optional in _p4,optional in _p5,optional in _p6,optional in _p7,optional in _p8,optional in _p9,optional in _p10)
     call to cubic spline with tension developed by O. Sauter using one extra input for the idl/C interface:
     From idl (or else) call wrapper:
     ==================================================================================================================
     interpos_wrapper_c(noptout,xin,yin,nin,yout[,tension,xout,nout,nbc,ybc,sigma,option,info[,youtp,youtpp,youtint]]])
     interpos_wrapper_c(noptout,xin,yin,nin,yout[,p0     ,p1  ,p2  ,p3 ,p4 ,p5   ,p6    ,p7  [,p8   ,p9    ,p10    ]]])
c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff,nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff,youtp,youtpp,youtint);
c_wrapper_to_interpos(nbarg ,noptout,xin,yin,nin,yout,p0       ,p1         ,p2      ,p3     ,p4     ,p5       ,p6        ,p7      ,p8   ,p9    ,p10)
     ==================================================================================================================
     which will call the f90 routine (nout required since uses explicit dim for portability):
     c_wrapper_to_interpos(nbarg,noptout,xin,yin,nin,yout,nout,tension,xout,nbc,ybc,sigma,option,info[,youtp,youtpp,youtint])
     . the arguments not provided to interpos_wrapper_c will be given with dummy values and not used in c_wrapper_to_interpos
     . the defaults will be decided within the f90 world
     
     Thus typical calls can be:
     interpos_wrapper_c(1,xin,yin,nin,yout); (to compute just the interpolation on xout=xin and tension=0, standard cubic spline)
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout); (to compute just the interpolation on xout)
     interpos_wrapper_c(3,xin,yin,nin,yout,tension,youtp,youtpp); (to compute just the fit and 1st, 2nd derivatives)
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout,[1 0],[0 0]); (to have B.C.: 1st der=0 at left, 2nd der=0 at right))
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout,[0 0],[0 0],sigma,63); (with sigma(1:nout)=1, to have y=yedge as extrapolation)
     (see http://spc.epfl.ch/interpos for more details)

     Adding one input as first argument to provide the number of desired output arrays:
     noptout = 1: only yout is calculated
     noptout = 2: yout and youtp (1st derivative) is computed
     noptout = 3: yout, youtp and youtpp (2nd derivative) is computed
     noptout = 4: yout, youtp, youtpp and youtint (integral) is computed

     Thus if noptout=1, one assumes youtp,youtpp and youtint are not provided 
     and if there are more arguments, nbc is present after yout

     argc is used to know if nbc, ybc, ... are provided. Default values are used if omitted
   The interface is actually performed in the f90 routine c_wrapper_to_interpos since it seems one can use the automatic match
     of the actual interpos called, depending on the type and number of arguments (in interpos module) only from a call within f90
     Thus here only call the f90 interface routine c_wrapper_to_interpos providing the number of arguments and noptout as extra parameters
     (it seems the f90 routine catches an extra input when using optional arguments and present() command, thus safer to use argc)

all:   interpos(in _server, in _shot, optional inout _logic)

        BPDuval: [CRPP, EPFL, Switzerland]      March 2016
*/
{
  _dummy_double = -9.d9;
  _dummy_long   = -99l;
/* nout_eff */
  if(!present(_p0))  _p0=_dummy_long;
/* tension_eff */
  if(!present(_p1))   _p1=_dummy_double;
/* xout_eff */
  if(!present(_p2))  _p2=_dummy_double;
/* nbc_eff */
  if(!present(_p3))  _p3=_dummy_long;
/* ybc_eff */
  if(!present(_p4))  _p4=_dummy_double;
/* sigma_eff */
  if(!present(_p5))  _p5=_dummy_double;
/* option_eff */
  if(!present(_p6))  _p6=_dummy_long;
/* info_eff */
  if(!present(_p7))  _p7=_dummy_long;
/* youtp */
  if(!present(_p8))  _p8=_dummy_double;
/* youtpp */
  if(!present(_p9))  _p9=_dummy_double;
/* youtint */
  if(!present(_p10)) _p10=_dummy_long;

/* 4 possible noptout
_s = interposgfortran_cwrapper->c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff,nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff,youtp,youtpp,youtint);
 */

 switch(_noptout) {
   case(1)
   if(_nbarg > 12 ) _nbarg =12;
  _s = interposgfortran_cwrapper->c_wrapper_to_interpos(ref(_nbarg),ref(_noptout),ref(_xin),ref(_yin),ref(_nin),ref(_yout),ref(_p0),ref(_p1),ref(_p2),ref(_p3),ref(_p4),ref(_p5),ref(_p6),ref(_p7));
        break;
   case(2)
  if(_nbarg > 13 ) _nbarg =13;
  _s = interposgfortran_cwrapper->c_wrapper_to_interpos(ref(_nbarg),ref(_noptout),ref(_xin),ref(_yin),ref(_nin),ref(_yout),ref(_p0),ref(_p1),ref(_p2),ref(_p3),ref(_p4),ref(_p5),ref(_p6),ref(_p7),ref(_p8));
        break;
   case(3)
  if(_nbarg > 14 ) _nbarg =14;
  _s = interposgfortran_cwrapper->c_wrapper_to_interpos(ref(_nbarg),ref(_noptout),ref(_xin),ref(_yin),ref(_nin),ref(_yout),ref(_p0),ref(_p1),ref(_p2),ref(_p3),ref(_p4),ref(_p5),ref(_p6),ref(_p7),ref(_p8),ref(_p9));
        break;
   case(4)
  if(_nbarg > 15 ) _nbarg =15;
  _s = interposgfortran_cwrapper->c_wrapper_to_interpos(ref(_nbarg),ref(_noptout),ref(_xin),ref(_yin),ref(_nin),ref(_yout),ref(_p0),ref(_p1),ref(_p2),ref(_p3),ref(_p4),ref(_p5),ref(_p6),ref(_p7),ref(_p8),ref(_p9),ref(_p10));
        break;
 }
return(_yout);
}
