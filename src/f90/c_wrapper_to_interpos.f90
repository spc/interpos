subroutine c_wrapper_to_interpos(nbarg,noptout,xin,yin,nin,yout,nout,tension,xout,nbc,ybc,sigma, &
  & option,info,youtp,youtpp,youtint) bind(c)
  !
  ! call c_wrapper_to_interpos(nbarg,noptout,xin,yin,nin,yout,nout &
  !                         & [,tension,xout,nbc,ybc,sigma,option,info,youtp,youtpp,youtint])
  !
  ! Assume the relevant arguments are the first (nbarg+1-noptout) variables, starting from noptout
  ! In addition, yout and youtp up to youtint are to be provided in output depending on the value of noptout=1, 2, 3 or 4 resp
  !
  ! Thus if nbarg=5 and noptout=1, actually only noptout,xin,yin,nin,yout,nout are used and need to be provided
  ! If nbarg=5 and noptout=3, one needs to call this routine with:
  ! - tension,xout,nbc,ybc,sigma,option,info containing dummy values with correct kind (but variables not used here)
  ! - and to provide memory space for youtp(nout),youtpp(nout), since they will be computed
  !
  USE iso_c_binding, only: c_double, c_int ! IGNORE
  USE prec_rkind
  USE interpos_module
  implicit none
  ! inputs/outputs
  integer(c_int) :: nbarg, noptout, nin, nout
  real(c_double) :: xin(nin), yin(nin), sigma(nin)
  real(c_double) :: yout(nout)
  integer(c_int), optional :: nbc(2), option, info
  real(c_double), optional :: tension, ybc(4)
  real(c_double), optional :: xout(nout), youtp(nout), youtpp(nout), youtint(nout)
  !
  integer :: nverbose, info_out
  !
  nverbose = 1
  !
  ! expect real to be of rkind and integers to be of size 4
  if (c_double .ne. rkind) then
    print *,'c_double= ',c_double,' is not equal to rkind= ',rkind
    print *,'check with olivier.sauter@epfl.ch'
    return
  end if
  if (c_int .ne. 4) then
    print *,'c_int= ',c_int,' is not equal to 4'
    print *,'check with olivier.sauter@epfl.ch'
    return
  end if
  if (nverbose .ge. 3) then
    print *,'in c_wrapper_to_interpos'
    print *,'nbarg= ',nbarg
    print *,'noptout= ',noptout
    print *,'nin, nout= ',nin,nout
    print *,'xin= ',xin
    print *,'size(xin)= ',size(xin)
    print *,'yin= ',yin
    if ((nbarg >=5) .or. (noptout>1)) then
      print *,'yout= ',yout
      print *,'size yout= ',size(yout)
      if ((nbarg >=6) .or. (noptout>1)) then
        print *,'tension= ',tension
        if ((nbarg >=8) .or. (noptout>1)) then
          print *,'xout= ',xout
          print *,'size xout= ',size(xout)
          if ((nbarg >=10) .or. (noptout>1)) then
            print *,'nbc= ',nbc
            print *,'size(nbc)= ',size(nbc)
            print *,'ybc= ',ybc
            print *,'size ybc= ',size(ybc)
            if ((nbarg >=11) .or. (noptout>1)) then
              print *,'sigma= ',sigma
              if ((nbarg >=12) .or. (noptout>1)) then
                print *,'option= ',option
                if ((nbarg >=13) .or. (noptout>1)) then
                  print *,'info= ',info
                end if
              end if
            end if
          end if
        end if
      end if
    end if
    if (noptout >=2) print *,'youtp= ',youtp
    if (noptout >=3) print *,'youtpp= ',youtpp
    if (noptout >=4) print *,'youtint= ',youtint
  end if

  if (nbarg .eq. 5) then
    if (noptout .eq. 1) call interpos(xin,yin,nin,nout,yout=yout,info=info_out)
    if (noptout .eq. 2) call interpos(xin,yin,nin,nout,yout=yout,youtp=youtp,info=info_out)
    if (noptout .eq. 3) call interpos(xin,yin,nin,nout,yout=yout,youtp=youtp,youtpp=youtpp,info=info_out)
    if (noptout .eq. 4) call interpos(xin,yin,nin,nout,yout=yout,youtp=youtp,youtpp=youtpp, &
      & youtint=youtint,info=info_out)
  else if (nbarg .eq. 6) then
    if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,yout=yout,info=info_out)
    if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,yout=yout,youtp=youtp,info=info_out)
    if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,yout=yout,youtp=youtp,youtpp=youtpp,info=info_out)
    if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,yout=yout,youtp=youtp,youtpp=youtpp, &
      & youtint=youtint,info=info_out)
  else if ((nbarg .eq. 7) .or. (nbarg .eq. 8)) then
    if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout,yout,info=info_out)
    if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout,&
      & yout,youtp,info=info_out)
    if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
      & yout,youtp,youtpp,info=info_out)
    if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
      & yout,youtp,youtpp,youtint,info=info_out)
  else if (nbarg .eq. 10) then
    if (nbc(1) .eq. -1) then
      ! since interpos_module picks up interpos with periodic condition if nbc of length (1), need to call it as such here
      ! and without option parameter
      if (noptout .eq. 1) call interpos(xin(1:nin),yin(1:nin),nin,nout,tension,xout(1:nout),yout(1:nout), &
        & nbc=nbc(1),ybc=ybc(1),info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc(1),ybc=ybc(1),info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc(1),ybc=ybc(1),info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc(1),ybc=ybc(1),info=info_out)
    else
      if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout,yout,nbc=nbc,ybc=ybc,info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc,ybc=ybc,info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc,ybc=ybc,info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc,ybc=ybc,info=info_out)
    end if
  else if (nbarg .eq. 11) then
    if (nbc(1) .eq. -1) then
      if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout,yout,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
    else
      if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout,yout,nbc=nbc,ybc=ybc,sigma=sigma,info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc,ybc=ybc,sigma=sigma,info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc,ybc=ybc,sigma=sigma,info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc,ybc=ybc,sigma=sigma,info=info_out)
    end if
  else if (nbarg .ge. 12) then
    if (nbc(1) .eq. -1) then
      ! option not an option for periodic boundary conditions since uses periodicity for extrapolation
      ! but implemented only for cubic spline interpolation. Thus add a warning if nverbose>=1 and if mod(option,10).ne.3
      if ((nverbose .ge. 1) .and. (mod(option,10) .ne. 3)) then
        print *,'WARNING: periodic boundary conditions implemented only for spline interpolation, thus uses cubics'
      end if
      if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc(1),ybc=ybc(1),sigma=sigma,info=info_out)
    else
      if (noptout .eq. 1) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,nbc=nbc,ybc=ybc,sigma=sigma,option=option,info=info_out)
      if (noptout .eq. 2) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,nbc=nbc,ybc=ybc,sigma=sigma,option=option,info=info_out)
      if (noptout .eq. 3) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,nbc=nbc,ybc=ybc,sigma=sigma,option=option,info=info_out)
      if (noptout .eq. 4) call interpos(xin,yin,nin,nout,tension,xout, &
        & yout,youtp,youtpp,youtint,nbc=nbc,ybc=ybc,sigma=sigma,option=option,info=info_out)
    end if
  else
    print *,'option nbarg = ',nbarg,' not available or not correct, ask Olivier.Sauter@epfl.ch'
    return
  end if
  return
  
end SUBROUTINE c_wrapper_to_interpos
