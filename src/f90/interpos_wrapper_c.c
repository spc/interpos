#include <stdio.h>

int interpos_wrapper_c(int argc,void *argv[])
{
  extern void c_wrapper_to_interpos();
  /* call to cubic spline with tension developed by O. Sauter using one extra input for the idl/C interface:
     From idl (or else) call wrapper:
     ==================================================================================================================
     interpos_wrapper_c(noptout,xin,yin,nin,yout[,tension,xout,nout,nbc,ybc,sigma,option,info[,youtp,youtpp,youtint]]])
     ==================================================================================================================
     which will call the f90 routine (nout required since uses explicit dim for portability):
     c_wrapper_to_interpos(nbarg,noptout,xin,yin,nin,yout,nout,tension,xout,nbc,ybc,sigma,option,info[,youtp,youtpp,youtint])
     . the arguments not provided to interpos_wrapper_c will be given with dummy values and not used in c_wrapper_to_interpos
     . the defaults will be decided within the f90 world
     
     Thus typical calls can be:
     interpos_wrapper_c(1,xin,yin,nin,yout); (to compute just the interpolation on xout=xin and tension=0, standard cubic spline)
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout); (to compute just the interpolation on xout)
     interpos_wrapper_c(3,xin,yin,nin,yout,tension,youtp,youtpp); (to compute just the fit and 1st, 2nd derivatives)
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout,[1 0],[0 0]); (to have B.C.: 1st der=0 at left, 2nd der=0 at right))
     interpos_wrapper_c(1,xin,yin,nin,yout,tension,xout,nout,[0 0],[0 0],sigma,63); (with sigma(1:nout)=1, to have y=yedge as extrapolation)
     (see http://spc.epfl.ch/interpos for more details)
  */
  /* 
     Adding one input as first argument to provide the number of desired output arrays:
     noptout = 1: only yout is calculated
     noptout = 2: yout and youtp (1st derivative) is computed
     noptout = 3: yout, youtp and youtpp (2nd derivative) is computed
     noptout = 4: yout, youtp, youtpp and youtint (integral) is computed

     Thus if noptout=1, one assumes youtp,youtpp and youtint are not provided 
     and if there are more arguments, nbc is present after yout

     argc is used to know if nbc, ybc, ... are provided. Default values are used if omitted
  */
  /* The interface is actually performed in the f90 routine c_wrapper_to_interpos since it seems one can use the automatic match
     of the actual interpos called, depending on the type and number of arguments (in interpos module) only from a call within f90
     Thus here only call the f90 interface routine c_wrapper_to_interpos providing the number of arguments and noptout as extra parameters
     (it seems the f90 routine catches an extra input when using optional arguments and present() command, thus safer to use argc)
  */
  int iarg, i, nverbose, nbargeff;
  long *nin, *nout, *nbc, *info, *option;
  short *noptout_short, *nin_short, *nout_short, *nbc_short, *info_short, *option_short;
  long *noptout, nbarg;
  double *xin,*yin,*xout,*yout,*youtp,*youtpp,*youtint,*sigma;
  double *ybc,*tension;
  long nout_eff;
  /* optional arguments need a dummy variable for memory placeholder */
  double dummy_double, *tension_eff, *xout_eff, *ybc_eff, *sigma_eff;
  long dummy_long, *nbc_eff, *option_eff, *info_eff;

  nverbose = 1; /* set it to 1 to have warnings, 0 for only errors and 3 for full messages */
  dummy_double = -9.e09;
  dummy_long = -99;

  if (nverbose >= 3) {printf("argc = %d\n",argc);}
  if (argc <5)
    {
      printf("problems, nb of input args = %d, expects at least 5 arguments: noptout,xin,yin,nin,yout\n",argc);
      return 1;
    }
  noptout = (long *) argv[0];
  noptout_short = (short *) argv[0];
  if ((int) *noptout_short != (int) *noptout) {
    /* note that for noptout=1 the above check does not work as both see 1 even if given as short in idl */
    /* but for integer values larger than 1, then there is a difference */
    printf("problem with noptout, seems was not defined as long, in interpos_wrapper_c:\nnoptout= %ld, noptout_short= %d\n",\
	   *noptout, *noptout_short);
    return 21; }
  if (nverbose >= 3) {printf("argv[0] = %d\n",(int)*noptout);}
  xin=(double *) argv[1];
  if (nverbose >= 3) {printf("xin(0,2)= %f, %f\n",xin[0],xin[2]);}
  yin=(double *) argv[2];
  nin=(long *) argv[3];
  nin_short=(short *) argv[3];
  if ((int) *nin_short != (int) *nin) {
    printf("problem with nin, seems was not defined as long, in interpos_wrapper_c:\nnin= %ld, nin_short= %d\n",*nin, *nin_short);
    return 22; }
  nout_eff = *nin;
  if (nverbose >= 3) {
    printf("nin= %d\n",*nin);
    printf("xin:\n");
    for (i=0; i<(int) *nin; i++) {
      printf("%d, %f ", i,xin[i]);
      printf("\n"); }
  }
  yout=(double *) argv[4];
  if (nverbose >= 3) { printf("yout[1]= %f\n",yout[1]);}
  iarg = 5; /* index of next input */

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    tension=(double *) argv[iarg]; iarg++;
    tension_eff = tension;}
  else {
    tension_eff = &dummy_double; }

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    xout=(double *) argv[iarg]; iarg++;
    xout_eff = xout;
    if (nbargeff < iarg) {
      if (nverbose >= 3) { printf("xout is provided without providing nout, assumes nout=nin, although xout is not necessarily xin\n\n");}
    }
  }
  else {
    xout_eff = xin;
    if (nverbose >= 3) {printf("nout_eff= %d\n",nout_eff); }
  }

  if (nverbose >= 3) { printf("argc-1= %d, iarg= %d, noptout= %d\n",argc-1,iarg,*noptout); }

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    if (nverbose >= 3) { printf("argc-1= %d, iarg= %d, noptout= %d\n",argc-1,iarg,*noptout); }
    nout=(long *) argv[iarg]; iarg++;
    nout_short=(short *) argv[iarg-1];
    if ((int) *nout_short != (int) *nout) {
      printf("problem with nout, seems was not defined as long, in interpos_wrapper_c:\nnout= %d, nout_short= %d\n",\
	     *nout, *nout_short);
      return 23; }
    nout_eff = *nout;
    if (nverbose >= 3) { printf("nout_eff from nout= %d\n",nout_eff);}
  }

  nbargeff=argc-1+1-*noptout;
  if ((nbargeff >= iarg) & (nbargeff < iarg+1)) {
    printf("problem with inputs, should provide ybc if you provide nbc\n");
    return 3;}
  if (nbargeff >= iarg+1) { /* if nbc provided, ybc should as well hence iarg+1 */
    nbc=(long *) argv[iarg]; iarg++;
    nbc_short=(short *) argv[iarg-1];
    if ((int) *nbc_short != (int) *nbc) {
      printf("problem with nbc, seems was not defined as long, in interpos_wrapper_c:\nnbc= %d, nbc_short= %d\n",*nbc, *nbc_short);
      return 24; }
    ybc=(double *) argv[iarg]; iarg++; 
    nbc_eff = nbc;
    ybc_eff = ybc;}
  else {
    nbc_eff = &dummy_long;
    ybc_eff = &dummy_double; }
  if (nverbose >= 3) { printf("iarg after ybc= %d\n",iarg);}

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    sigma=(double *) argv[iarg]; iarg++;
    sigma_eff = sigma; }
  else {
    sigma_eff = &dummy_double; }

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    option=(long *) argv[iarg]; iarg++;
    option_short=(short *) argv[iarg-1];
    if ((int) *option_short != (int) *option) {
      printf("problem with option, seems was not defined as long, in interpos_wrapper_c:\noption= %d, option_short= %d\n",*option, *option_short);
      return 25; }
    option_eff = option;}
  else {
    option_eff = &dummy_long; }

  nbargeff=argc-1+1-*noptout;
  if (nbargeff >= iarg) {
    info=(long *) argv[iarg]; iarg++;
    info_short=(short *) argv[iarg-1];
    if ((int) *info_short != (int) *info) {
      printf("problem with info, seems was not defined as long, in interpos_wrapper_c:\ninfo= %d, info_short= %d\n",*info, *info_short);
      return 26; }
    info_eff = info;}
  else {
    info_eff = &dummy_long; }
  if (nverbose >= 3)
    printf("nout_eff= %d\nnoptout= %d\n",nout_eff,(int)*noptout);
  if ((int)*noptout >= 2) {
    if (nverbose >= 3) {printf("iarg for youtp= %d\n",iarg);}
    youtp=(double *) argv[iarg]; iarg++;}
  if ((int)*noptout >= 3) {youtpp=(double *) argv[iarg]; iarg++;}
  if ((int)*noptout >= 4) {youtint=(double *) argv[iarg]; iarg++;}

  if (nverbose >= 3)
    printf("argc-1= %d, iarg+1= %d\nargc+1-*noptout=  %d\n",argc-1,iarg+1,argc+1-(int)*noptout);
  nbarg = argc+1-*noptout;
  if (nverbose >= 3) { printf("nbarg= %d\n",nbarg);}
  switch((int)*noptout) {
  case 1:
    c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff, \
                          nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff);
    break;
  case 2:
    c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff, \
                          nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff, \
                          youtp);
    break;
  case 3:
    c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff, \
                          nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff, \
                          youtp,youtpp);
    break;
  case 4:
    c_wrapper_to_interpos(&nbarg,noptout,xin,yin,nin,yout,&nout_eff,tension_eff,xout_eff, \
                          nbc_eff,ybc_eff,sigma_eff,option_eff,info_eff, \
                          youtp,youtpp,youtint);
    break;
  default:
    printf("Option noptout = [%d] unknown\n",(int)*noptout);
  }
  
}
