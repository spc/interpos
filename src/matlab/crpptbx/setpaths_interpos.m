%
% set paths to interpos according to hostname
%
% JDR 2023.11.03
%
mfd = fileparts(mfilename('fullpath'));

paths_next = fullfile(mfd,'next');
paths_default = fullfile(mfd,'default');

% Check if shared libraries are found for default
[res,~] = system(['ldd ',fullfile(paths_default,['interpos.',mexext]),' | grep -q "not found"']);
if res ~= 0
  addpath(paths_default);
  return
end

% Check if shared libraries are found for next
[res,~] = system(['ldd ',fullfile(paths_next,['interpos.',mexext]),' | grep -q "not found"']);
if res ~= 0
  addpath(paths_next);
  return
end

% If none were found, print a warning message
fprintf('[interpos] No valid interpos MEX-file found\n')
