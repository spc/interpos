#!/bin/bash
# MATLAB/OCTAVE testing script for Continuous Integration
# F. Felici federico.felici@epfl.ch

## test type
if [[ -z "$1" ]]
then
  echo 'usage:'
  echo ' Run matlab tests:'
  echo '     ./test_script.sh $matlabcmd $test_to_run'
  echo '     will call required matlab and run >>tests_matlab($test_to_run)'
  echo '   $matlabcommand is the command used to launch the correct matlab version'
  echo '   works on SPC lac clusters for now'
  echo ' Run octave tests:'
  echo '     ./test_script.sh $octavebin $testfile'
  echo '     will call required octave and run tests_octave($testfile)'
  exit 1
fi

matlabbin=$1
testargument=$2

if [[ $(basename -- $matlabbin) != octave ]]; then
  # MATLAB
  call="tests_matlab('$testargument')"
  if [[ $OS = "Windows_NT" ]]
  then
    full_args="-batch"
  else
    matlabargs="-singleCompThread -nodesktop -nosplash -noFigureWindows"
    full_args="$matlabargs -r"
  fi
else
  # OCTAVE
  call="tests_octave('$testargument')"
  full_args="--no-gui --eval"
  # So far no benefit from using more than 1 thread with Octave
  export OMP_NUM_THREADS=1
fi

echo \"$matlabbin\" $full_args \"$call\"

exec "$matlabbin" $full_args "$call" ## execute