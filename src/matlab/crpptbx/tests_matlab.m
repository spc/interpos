function tests_matlab(test_case)
try
   fprintf('\n Running test file: %s\n',mfilename('fullpath'));
   fprintf('     Time: %s\n',datestr(now));

   setpaths_interpos; 
   test_interpos;
   failed = exist('ME','var')>0;
   exit_code = int32(failed); % convert to bash shell convention
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
