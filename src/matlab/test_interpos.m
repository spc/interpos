n_t = 0; % Test counter
n_tests = 7; % Number of tests
fprintf('\n1..%d\n',n_tests);
as_ok={'not ok';'ok'}; % array of strings

n_t = n_t+1; % Subsequent test
try
  s_which = which('interpos');
  b_test = ~isempty(s_which);
  fprintf('%s %d - which interpos is %s\n', as_ok{1+(b_test)}, n_t, s_which)
catch ME
  fprintf('%s %d - which interpos failed: %s\n','not ok',n_t, ME.message);
end

n_t = n_t+1; % Subsequent test
try
  s_help = help('interpos');
  ss_help = size(s_help); ss_help=max(ss_help(:));
  b_test = ~isempty(s_help) && ss_help > 1;
  fprintf('%s %d - size help interpos is %d\n', as_ok{1+(b_test)}, n_t, ss_help)
catch ME
  fprintf('%s %d - size help interpos failed: %s\n', 'not ok', n_t, ME.message)
end

x=linspace(0,6*pi,50000);
xspl2=linspace(-3*pi,9*pi,3000000);
taus=1e-6;

n_t = n_t+1; % Subsequent test
try
  tic;[atauper,btauper,ctauper,d1]=interpos(13,x(1:end-1),sin(x(1:end-1)).^2,xspl2,taus,-1,6.*pi);te=toc;
  yanal = sin(xspl2).^2;
  % Should complete in less than 10.0 s ( some extra time for the first -- time to cache libs etc.)
  % b_test = te > 0 && te < 10.0;
  err_interpos = sum(abs(atauper-yanal))./length(xspl2);
  b_test = err_interpos < 1.e-4;
  fprintf('%s %d - interpos average diff with sin(x)^2 is %e\n', as_ok{1+(b_test)}, n_t, err_interpos)
catch ME
  fprintf('%s %d - interpos large nb points sin test failed on try: %s\n', 'not ok', n_t, ME.message)
end

n_t = n_t+1; % Subsequent test
try
  tic;[atauper,btauper,ctauper,d]=interpos(13,x(1:end),sin(x(1:end)).^2,xspl2,taus,[2 2],[0 0]);te=toc;
  yanal = 2.*sin(xspl2).*cos(xspl2);
  % Should complete in less than 1.0 s
  %b_test = te > 0 && te < 1.0;
  ij=find(xspl2>=x(1)&xspl2<=x(end));
  err_interpos = sum(abs(btauper(ij)-yanal(ij)))./length(xspl2(ij));
  b_test = err_interpos < 1.e-4;
  %fprintf('%s %d # TODO: replace time test. interpos elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, te)
  fprintf('%s %d - interpos d/dx av.diff with 2sin cos is %e, elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, err_interpos,te)
catch ME
  fprintf('%s %d - interpos try d/dx failed: %s\n', 'not ok', n_t, ME.message)
end

xspl3=linspace(0.1,5.9*pi,3000000);

n_t = n_t+1; % Subsequent test
try
  tic;[atauper,btauper,ctauper,d1]=interpos(3,x(1:end-1),sin(x(1:end-1)).^2,xspl3,taus,-1,6.*pi);te=toc;
  % Should complete in less than 1.0 s
  %b_test = te > 0 && te < 1.0;
  yanal = 2.*cos(xspl3).*cos(xspl3) - 2.*sin(xspl3).*sin(xspl3);
  err_interpos = sum(abs(ctauper-yanal))./length(xspl2);
  b_test = err_interpos < 1.e-4;
  %fprintf('%s %u # TODO: replace time test. interpos elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, te)
  fprintf('%s %d - interpos d2/dx2 av.diff with 2(cos^2-sin^2) is %e, elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, err_interpos,te)
catch ME
  fprintf('%s %d - interpos try d2/dx2 failed: %s\n', 'not ok', n_t, ME.message)
end

n_t = n_t+1; % Subsequent test
try
  tic;[atauper,btauper,ctauper,d]=interpos(3,x(1:end),sin(x(1:end)).^2,xspl3,taus,[2 2],[0 0]);te=toc;
  % Should complete in less than 1.0 s
  %b_test = te > 0 && te < 1.0;
  ij=find(xspl3>=x(1)&xspl3<=x(end));
  yanal = xspl3/2 - sin(2.*xspl3)/4;
  err_interpos = sum(abs(d-yanal))./length(xspl2);
  b_test = err_interpos < 1.e-4;
  %fprintf('%s %d # TODO: replace time test. interpos elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, te)
  fprintf('%s %d - interpos integral av.diff with x/2 - sin(2.*x)/4 is %e, elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, err_interpos,te)
catch ME
  fprintf('%s %d - interpos try integral failed: %s\n', 'not ok', n_t, ME.message)
end

n_t = n_t+1; % Subsequent test
try
  x(1) = Inf; x(end) = NaN;
  tic;[atauper,btauper,ctauper,d]=interpos_nan(3,x(1:end),sin(x(1:end)).^2,xspl3,taus,[2 2],[0 0]);te=toc;
  % Should complete in less than 1.0 s
  %b_test = te > 0 && te < 1.0;
  ij=find(xspl3>=x(1)&xspl3<=x(end));
  yanal = xspl3/2 - sin(2.*xspl3)/4;
  err_interpos = sum(abs(d-yanal))./length(xspl2);
  b_test = err_interpos < 1.e-4;
  %fprintf('%s %d # TODO: replace time test. interpos elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, te)
  fprintf('%s %d - interpos integral av.diff with x/2 - sin(2.*x)/4 is %e, elapsed time is %d seconds\n', as_ok{1+(b_test)}, n_t, err_interpos,te)
catch ME
  fprintf('%s %d - interpos try integral failed: %s\n', 'not ok', n_t, ME.message)
end
