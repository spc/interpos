from numpy.distutils.core import setup, Extension

ext1 = Extension(name = 'py_interpos.interpos_fun',
                 sources = ['interpos_fun.pyf','interpos.f90']
                 )

setup(name         = 'py_interpos',
      version      = '8.3',
      description  = "Python interface to the interpos library",
      author       = "Antoine Merle",
      author_email = "antoine.merle@epfl.ch",
      url          = "https://crppwww.epfl.ch/~sauter/interpos/",
      ext_modules  = [ext1],
      packages     = ['py_interpos'],
      package_dir  = {'py_interpos':''},
      package_data = {'py_interpos':['README.txt']}
      )
