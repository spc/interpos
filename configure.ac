AC_PREREQ([2.68])
AC_INIT([INTERPOS],
   m4_esyscmd([build-aux/git-version-gen --prefix '' .tarball-version]),
   [imas-support@iter.org], [INTERPOS], [https://crppwww.epfl.ch/~sauter/interpos/])

## Add libtool shared library version numbers to the library
## See libtool versioning documentation online and below.
## Here, Simply pick 10*MajorVersion + MinorVersion = Interface version
LT_F_VERS_INTERFACE=85
LT_F_VERS_REVISION=0
LT_F_VERS_AGE=0
AC_SUBST(LT_F_VERS_INTERFACE)
AC_SUBST(LT_F_VERS_REVISION)
AC_SUBST(LT_F_VERS_AGE)
## If the API changes *at all*, increment LT_VERS_INTERFACE and
## reset LT_VERS_REVISION to 0.
##
## If the API changes but no function signatures are removed or
## changed, also increment LT_VERS_AGE.
## If any functions are removed from the API, or their signatures
## are changed reset LT_VERS_AGE to 0 to indicate that previous
## versions of the API are not necessarily compatible with this
## version.
##
## If the source changes but there are no API changes, increment
## LT_VERS_REVISION.

## Expect this file to identify the src dir.
AC_CONFIG_SRCDIR([src/f90/cbfitbnd.f90])

## Auxiliary build tools are to found here. (e.g., install-sh, config.sub, config.guess) and M4 files.
AC_CONFIG_AUX_DIR([build-aux])
AC_REQUIRE_AUX_FILE([tap-driver.sh])
AC_CONFIG_MACRO_DIR([m4])
m4_include([m4/ax_check_compile_flag.m4])
m4_include([m4/ax_f90_module_extension.m4])
m4_include([m4/ax_f90_module_flag.m4])
m4_include([m4/ax_pkg_check_modules.m4])
m4_include([m4/pkg.m4])
m4_include([m4/ax_blas.m4])
m4_include([m4/ax_matlab.m4])

## AM_ MAINTAINER_MODE turns off maintainer-only makefile targets by default, and changes configure to understand a
## --enable-maintainer-mode option. AM_MAINTAINER_MODE makes it a bit harder for users to shoot themselves in the foot.
AM_MAINTAINER_MODE

AM_INIT_AUTOMAKE([1.6 foreign subdir-objects])
## Use one of the Fortran compilers, set it as default language.
AC_PROG_FC([gfortran ifort pgfortran g95])
AC_LANG(Fortran)
## Warnings count as fatal errors for the current language.
AC_LANG_WERROR
AC_FC_SRCEXT(f90)
## The AC_ FC_FREEFORM tries to ensure that the Fortran compiler ($FC) allows free-format source code (as opposed to the older fixed-format style from Fortran 77). If necessary, it may add some additional flags to FCFLAGS.
AC_FC_FREEFORM
## The AC_FC_LINE_LENGTH macro tries to ensure that the Fortran compiler ($FC) accepts long source code lines
AC_FC_LINE_LENGTH
#AC_ FC_LIBRARY_LDFLAGS

## Find Fortran 90 modules inclusion flag. ($ax_cv_f90_modflag)
AX_F90_MODULE_FLAG
if test "x$ax_cv_f90_modflag" != x; then
  FC_MODFLAG=$ax_cv_f90_modflag
else
  AC_MSG_ERROR([Failure determining Fortran module flag])
fi
AC_SUBST([FC_MODFLAG])
## Find out the file name extensions of Fortran module files. ($ax_cv_f90_modext)
AX_F90_MODULE_EXTENSION
if test "x$ax_cv_f90_modext" != x; then
  FC_MODEXT=$ax_cv_f90_modext
else
  AC_MSG_ERROR([Failure determining Fortran module file extension])
fi
AC_SUBST([FC_MODEXT])

# Add -m64 -fdefault-real-8 flags
AX_CHECK_COMPILE_FLAG([-m64], [FCFLAGS="$FCFLAGS -m64"])
if test x$FC = xifort; then
  AX_CHECK_COMPILE_FLAG([-r8], [FCFLAGS="$FCFLAGS -r8"])
fi
if test x$FC = xgfortran; then
  AX_CHECK_COMPILE_FLAG([-fdefault-real-8], [FCFLAGS="$FCFLAGS -fdefault-real-8"])
fi
if test x$FC = xg95; then
  AX_CHECK_COMPILE_FLAG([-fdefault-real-8], [FCFLAGS="$FCFLAGS -fdefault-real-8"])
fi
if test x$FC = xpgfortran; then
  AX_CHECK_COMPILE_FLAG([-r8], [FCFLAGS="$FCFLAGS -r8"])
fi
# Same for CFLAGS
AC_LANG_PUSH([C])
AX_CHECK_COMPILE_FLAG([-m64], [CFLAGS="$CFLAGS -m64"])
AC_LANG_POP

# AM_CFLAGS is an automake construct which should be used by Makefiles
# instead of CFLAGS, as CFLAGS is reserved solely for the user to define.
# This applies to FCFLAGS, CXXFLAGS, CPPFLAGS, and LDFLAGS as well.
AC_SUBST(AM_CFLAGS)
AC_SUBST(AM_FCFLAGS)
AC_SUBST(AM_CPPFLAGS)
AC_SUBST(AM_LDFLAGS)

## Check which archiving tool to use. This needs to be done before
## the AM_PROG_LIBTOOL macro.
if test -z "$AR"; then
  AC_CHECK_PROGS([AR], [ar xar], [:], [$PATH])
fi
AC_SUBST([AR])
## Export the AR macro so that it will be placed in the libtool file
## correctly.
export AR

AC_PROG_MAKE_SET
AC_PROG_INSTALL
AC_PATH_PROG([TR], [tr])
AC_PATH_PROG([TAIL], [tail])
AC_PATH_PROG([SED], [sed])

## Create libtool.
LT_PREREQ([2.2])
## dlopen - This will use an improved version of libtool
## win32-dll - This will build clean dlls on win32 platforms.
LT_INIT([dlopen,win32-dll])
## Check for system libraries. "dl" stands for dynamically loaded library
## AC_CHECK_LIB([dl], [dlopen])
## AC_CHECK_LIB([m], [ceil])

## Use pkg-config
PKG_PROG_PKG_CONFIG
PKG_INSTALLDIR

## Use IEEE_ARITHMETIC F90 module or not
AC_ARG_WITH([ieee],
            [AS_HELP_STRING([--without-ieee], [ignore IEEE_ARITHMETIC intrinsic module and use internal])])
AS_IF([test "x$with_ieee" != "xno"],[], [
  FCFLAGS="$FCFLAGS -DNO_IEEE"
  ])
AM_CONDITIONAL([HAVE_IEEE], [test "x$with_ieee" != "xno"])

## Detect system Matlab and mex commands
AX_MATLAB

## Detect system LAPACK library, or use internal functions
AC_ARG_WITH([lapack],
            [AS_HELP_STRING([--without-lapack], [ignore LAPACK/BLAS libs and use internal])])
AS_IF([test "x$with_lapack" != "xno"], [], [
  with_blas="no"
  with_lapack="no"
  ])
## Looks like openblas has pkg-config file, so use it
## and intel mkl has them too, but hidden, so don't expect them.
AS_CASE([$with_blas],
    [no],[AM_CONDITIONAL([HAVE_openblas],[false])],
    [PKG_HAVE_WITH_MODULES([openblas],[openblas >= 0.2])])
##PKG_HAVE_WITH_MODULES([mkl-dynamic-lp64-seq],[mkl-dynamic-lp64-seq >= 2018], [])
AS_IF([test "x$with_openblas" = "xyes"],[
       ax_package_requires="openblas >= 0.2" # have pkg-config support
       ax_blas_ok=yes # disables testing later
       ax_lapack_ok=yes # disables testing later
      ])
## If pkg-config openblas failed, discover BLAS/LAPACK
AS_IF([test "x$with_openblas" = "xno"],[AX_BLAS()])
AS_IF([test "x$with_openblas" = "xno"],[AX_LAPACK()])
## Add whatever AX_BLAS discovered to pc file
## and what the Intel MKL Link Line Advisor says (for a few cases)
AS_CASE([$BLAS_LIBS],
  [-lmkl_gf_*lp64*],[
	package_extra_libs="-Wl,--no-as-needed $LAPACK_LIBS $BLAS_LIBS $LIBS"
	package_extra_flags="-m64"
	],
  [-lmkl*lp64*],[
	package_extra_libs="$LAPACK_LIBS $BLAS_LIBS $LIBS"
	package_extra_flags=""
	],
  [-l*],  [package_extra_libs="$LAPACK_LIBS $BLAS_LIBS $LIBS"],
  [""],   [],
  )
AM_CONDITIONAL([HAVE_BLAS], [test x"$ax_blas_ok" = xyes])
AM_CONDITIONAL([HAVE_LAPACK], [test x"$ax_lapack_ok" = xyes])

## Substitute these for use in .pc file.
AC_SUBST([AX_PACKAGE_REQUIRES],[$ax_package_requires])
AC_SUBST([PACKAGE_EXTRA_LIBS],[$package_extra_libs])
AC_SUBST([PACKAGE_EXTRA_FLAGS],[$package_extra_flags])

#### Option to build examples
##AC_ARG_ENABLE([examples-f90],
##    AS_HELP_STRING([--enable-examples-f90], [Enable building of Fortran examples]))
##AM_CONDITIONAL([ENABLE_EXAMPLES_F90], [test x$enable_examples_f90 = xyes])
##AC_ARG_ENABLE([examples-matlab],
##    AS_HELP_STRING([--enable-examples-matlab], [Enable building of Matlab examples]))
##AM_CONDITIONAL([ENABLE_EXAMPLES_MAT], [test x$enable_examples_matlab = xyes])
##AC_ARG_ENABLE([examples-python],
##    AS_HELP_STRING([--enable-examples-python], [Enable building of Python examples]))
##AM_CONDITIONAL([ENABLE_EXAMPLES_PY], [test x$enable_examples_python = xyes])

## Option to disable f90
AC_ARG_ENABLE([f90],
    AS_HELP_STRING([--disable-f90], [Disable building f90 library]))
AM_CONDITIONAL([ENABLE_F90], [test "x$enable_f90" != "xno"])
## Option to disable tests
AC_ARG_ENABLE([tests],
    AS_HELP_STRING([--disable-tests], [Disable tests (i.e. make check)]))
AM_CONDITIONAL([ENABLE_TESTS], [test "x$enable_tests" != "xno"])
## Option to build C wrapper
AC_ARG_ENABLE([cwrapper],
    AS_HELP_STRING([--enable-cwrapper], [build cwrapper for IDL and friends]))
AM_CONDITIONAL([ENABLE_CWRAPPER], [test x$enable_cwrapper = xyes])

#### Option to install modulefile, and where
##AC_ARG_ENABLE([modulefile],
##    AS_HELP_STRING([--enable-modulefile], [install modulefile (default no)]))
##AM_CONDITIONAL([ENABLE_MODULEFILE], [test x$enable_modulefile = xyes])
##AC_ARG_WITH([modulefilesdir],
##            [AS_HELP_STRING([--with-modulefilesdir], [modulefiles installation directory ['${sysconfdir}/modulefiles']])],
##            [modulefilesdir=$withval],
##            [modulefilesdir="\${sysconfdir}/modulefiles"])
##AC_SUBST([MODULEFILESDIR], [$modulefilesdir])
## Option to override fortran modules dir
AC_ARG_WITH([fortranmoddir],
            [AS_HELP_STRING([--with-fortranmoddir], [Fortran .mod files installation directory ['${includedir}']])],
            [fortranmoddir=$withval],
            [fortranmoddir="\${includedir}"])
AC_SUBST([FORTRANMODDIR], [$fortranmoddir])

## ## Some extra substitutions for in .pc and modulefile generation.
## ## Set PACKAGE_ COMPILER variable if not set already.
## if test x$PACKAGE_COMPILER = x; then
##   PACKAGE_COMPILER=$FC
## fi
## if test x$PACKAGE_COMPILER_VERSION = x; then
##   PACKAGE_COMPILER_VERSION=`$FC --version | sed 's/.*\s\([[0-9]]\+\.[[0-9]]\+\.[[0-9]]\+\).*$/\1/g;1q'`
## fi
## ## Set PACKAGE_ COMPILER_MOD variable if not set already.
## ## (e.g. usually, the PGI module provides pgfortran, GCC provides gfortran, etc)
## if test x$enable_modulefile = xyes ;then
## AC_MSG_CHECKING([INTERPOS module name])
## if test x$PACKAGE_NAME_MOD = x; then
##   PACKAGE_NAME_MOD=INTERPOS
## fi
## AC_MSG_RESULT([$PACKAGE_NAME_MOD])
## AC_MSG_CHECKING([compiler module name/version])
## if test x$PACKAGE_COMPILER_MOD = x; then
##   AS_CASE([$FC],
##   [ifort],    [PACKAGE_COMPILER_MOD=$FC],
##   [gfortran], [PACKAGE_COMPILER_MOD=GCC],
##   [pgfortran],[PACKAGE_COMPILER_MOD=PGI],
##   [g95],      [PACKAGE_COMPILER_MOD=$FC],
##   [PACKAGE_COMPILER_MOD=$PACKAGE_COMPILER],
##   )
## fi
## ## Discover compiler module version
## if test x$PACKAGE_COMPILER_MOD_VER = x; then
##   PACKAGE_COMPILER_MOD_VER=`echo $LOADEDMODULES | $TR ':' '\n' | grep "^$PACKAGE_COMPILER_MOD/" | cut -d/ -f2`
## fi
## ## If that didnt work, the modulefile will probably be bad
## if test x$PACKAGE_COMPILER_MOD_VER = x ;then
##   PACKAGE_COMPILER_MOD_VER=$PACKAGE_COMPILER_VERSION
##   AC_WARN([Could not find compiler module/version. Try setting PACKAGE_COMPILER_MOD to match module name.])
## else
##   AC_MSG_RESULT([$PACKAGE_COMPILER_MOD/$PACKAGE_COMPILER_MOD_VER])
## fi
## fi # enable_modulefile

PACKAGE_DESCRIPTION="INTERPOS library for interpolating data"
AC_SUBST([PACKAGE_DESCRIPTION])
## Package libname is used in <libname>.pc, and is equal to lower-case PACKAGE_ NAME
PACKAGE_LIBNAME=`echo $PACKAGE_NAME | $TR [A-Z] [a-z]`
AC_SUBST([PACKAGE_LIBNAME])
## AC_ARG_VAR([PACKAGE_NAME_MOD],[module name [INTERPOS]])
## AC_ARG_VAR([PACKAGE_COMPILER_MOD],[module providing compiler [auto: 'PGI', 'GCC' or '$FC')]])
## AC_ARG_VAR([PACKAGE_COMPILER_MOD_VER],[version of compiler module [obtained from $LOADEDMODULES]])

## Output files to generate
AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([src/f90/Makefile src/f90/interpos.pc tests/f90/Makefile])
AC_CONFIG_FILES([src/matlab/Makefile])

## ## Generate Environment Module file. (with compiler in version)
## if test x$enable_modulefile = xyes ;then
## AC_CONFIG_FILES([scripts/modulefiles/${PACKAGE_NAME_MOD}/${PACKAGE_VERSION}-${PACKAGE_COMPILER_MOD}-${PACKAGE_COMPILER_MOD_VER}:scripts/modulefiles/${PACKAGE_NAME}/template-compiler-x.y.in])
## fi
AC_OUTPUT
