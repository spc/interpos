Authorisation to use and apply:

**INTERPOS**

at the _______________________________________________ (insert association name)

___

The INTERPOS modules are developed at the Swiss Plasma Center, Ecole Polytechnique Fédérale de Lausanne (SPC/EPFL), Switzerland.

INTERPOS is a versatile set of interpolation routines including cubic splines with tension, for which different boundary conditions and error bars can be specified.

The undersigned has received a copy of INTERPOS, including its source code, under
the additional conditions that:

1. The code does not change its name even if modified.

2. The undersigned agrees with the contributor license agreement (CLA), see
   [CLA_individual_INTERPOS.md](CLA_individual_INTERPOS.md)/[CLA_organization_INTERPOS.md](CLA_organization_INTERPOS.md) (v2.2) for details,
   also in the git repository license subfolder. Modifications of the codes
   that are developed and made available to the SPC are granting the right to
   EPFL to make these modifications available on an “as is” basis to all other
   holders of an agreed usage of INTERPOS.

3. Results produced with the original or the modified versions of INTERPOS should
   appropriately reference the original publications:

     O. Sauter, SPC-EPFL, private communication (a publication is under development)

4. INTERPOS can be used by the partners of the above association on the
   association related clusters under the above conditions. The partners can
   obtain the sources to be installed at their home institutes by just signing
   the code transfer document ([printable pdf version](https://crppwww.epfl.ch/~sauter/INTERPOS/Code_transfer_INTERPOS.pdf)).


5. INTERPOS nor their progeny may be transferred or made available to other
   research groups without the written authorisation from the SPC.

Responsible person

- Name: __________________________________

- email: __________________________________

- Place and Date: __________________________________

- Signature: __________________________________


___

SPC-EPFL<br/>
Station 13<br/>
1015 Lausanne (Switzerland)

http://spc.epfl.ch/INTERPOS
