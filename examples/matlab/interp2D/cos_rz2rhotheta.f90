subroutine cos_rz2rhotheta(equi_in,equi_out,nfluxout,ntheta_out,flagadd,krho_option)

  use ids_schemas
  use ids_convention
  implicit none

  type(ids_equilibrium)            :: equi_in,equi_out
  integer,intent(in)               :: nfluxout,ntheta_out,flagadd
  integer,optional                 :: krho_option
  integer                          :: dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd,ireg,i, irho_option_eff
  interface
     subroutine cos_rz2rhotheta_calc(equi_in,equi_out,nfluxout,ntheta_out,dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd,flagadd,krho_option)
       use ids_schemas
       implicit none
       type(ids_equilibrium)               :: equi_in,equi_out
       integer,intent(in)                  :: nfluxout,ntheta_out,flagadd, krho_option
       integer,intent(in)                  :: dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd
     end subroutine cos_rz2rhotheta_calc
  end interface

  ireg=0
  do i=1,size(equi_in%time_slice(1)%profiles_2d)
    if (equi_in%time_slice(1)%profiles_2d(i)%grid_type%index==ids_rectangular_grid) then
      ireg=i
    endif
  enddo
  if (ireg==0) then
    write(0,*) "cos_rz2rhotheta: no regular 2D profiles found"
    stop
  endif
  sizeRR  = size(equi_in%time_slice(1)%profiles_2d(ireg)%grid%dim1)
  sizeZZ  = size(equi_in%time_slice(1)%profiles_2d(ireg)%grid%dim2)
  sizeRbnd= size(equi_in%time_slice(1)%boundary%lcfs%r)
  dimphi1 = size(equi_in%time_slice(1)%profiles_2d(ireg)%phi,1)
  dimphi2 = size(equi_in%time_slice(1)%profiles_2d(ireg)%phi,2)
  ! krho_option: rhotor_norm by default (=0)
  irho_option_eff = 0
  if (present(krho_option)) then
    irho_option_eff = krho_option
  end if
  call cos_rz2rhotheta_calc(equi_in,equi_out,nfluxout,ntheta_out,dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd,flagadd,irho_option_eff)

end subroutine cos_rz2rhotheta

subroutine cos_rz2rhotheta_calc(equi_in,equi_out,nfluxout,ntheta_out,dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd,flagadd,krho_option) 
  !
  ! krho_option =  0: rho_tor_norm as radial coordinate, equidistant in Phi
  ! krho_option =  1: rho_tor as radial coordinate, equidistant in Phi
  ! krho_option = 10: rho_tor_norm as radial coordinate but equidistant mesh in rho_tor_norm
  ! krho_option = 11: rho_tor as radial coordinate but equidistant mesh in rho_tor
  !
  use interpos_module
  use ids_schemas
  use ids_routines
  use ids_convention
  !use copy_structures
  !use deallocate_structures
  implicit none

  type(ids_equilibrium)                   :: equi_in,equi_out
  integer,intent(in)                      :: nfluxout,ntheta_out
  integer,intent(in)                      :: dimphi1,dimphi2,sizeRR,sizeZZ,sizeRbnd,flagadd,krho_option
  real*8,dimension(:),pointer             :: RR,ZZ,Rbnd,Zbnd
  real*8                                  :: Raxis,Zaxis,B0
  real*8,dimension(dimphi1,dimphi1)       :: phi_rz,flux_rz_norm
  real*8,dimension(ntheta_out)            :: thetamesh,rhobound_thetamesh
  real*8,dimension(nfluxout)              :: flux_norm_out
  real*8,dimension(sizerbnd)              :: rhobnd,thetabnd
  real*8,dimension(:),allocatable         :: rhobndtmp,thetabndtmp,Rbndtmp,Zbndtmp
  integer                                 :: sizeRbndtmp
  real*8                                  :: phi_edge,aux1,aux2,tension_default
  real*8,dimension(sizeRR,sizeZZ,4)       :: farray_in
  real*8,dimension(nfluxout,ntheta_out,4) :: farray_out,farray_out_fluxnormtheta
  real*8,dimension(nfluxout,ntheta_out)   :: rhopolar_at_flux,Rrhotheta,Zrhotheta,thetamesh2Dnew, &
    & Rrhotheta_atflux,Zrhotheta_atflux
  integer                                 :: i,j,k,nsigma,iprofile
  real*8, PARAMETER                       :: PI=3.141592653589793238462643383279502884197_8
  real*8,dimension(nfluxout)              :: sigma
  real*8,dimension(nfluxout,ntheta_out)   :: rhomesh,thetamesh2D
  integer                                 :: profiles_2d_size,ireg

  interface
     subroutine cos_interpos2d_cartesian(RR,ZZ,f2darray_in,Rout,Zout,f2darray_out,tension_in,kextrapol)
       use interpos_module
       IMPLICIT NONE
       real*8, intent(IN)                :: RR(:), ZZ(:), f2darray_in(:,:,:), Rout(:,:), Zout(:,:)
       real*8, intent(IN), optional      :: tension_in
       integer, intent(IN), optional     :: kextrapol
       real*8                            :: f2darray_out(:,:,:)
     end subroutine cos_interpos2d_cartesian

  end interface

  tension_default = -0.1_8
  nsigma=nfluxout
  rhopolar_at_flux=0.0

  ireg=0
  do i=1,size(equi_in%time_slice(1)%profiles_2d)
    if (equi_in%time_slice(1)%profiles_2d(i)%grid_type%index == ids_rectangular_grid) then
      ireg=i
    endif
  enddo
  if (ireg==0) then
    write(0,*) "cos_rz2rhotheta: no regular 2D profiles found"
    stop
  endif

  call ids_copy(equi_in,equi_out)
  if (flagadd==1) then
    profiles_2d_size=size(equi_out%time_slice(1)%profiles_2d)
    do i=1,profiles_2d_size
      call ids_deallocate(equi_out%time_slice(1)%profiles_2d(i))
    enddo
    allocate(equi_out%time_slice(1)%profiles_2d(profiles_2d_size+1))
    do i=1,profiles_2d_size
      call ids_copy(equi_in%time_slice(1)%profiles_2d(i),equi_out%time_slice(1)%profiles_2d(i))
    enddo
  endif
  B0  = equi_in%vacuum_toroidal_field%b0(1)

  RR  =>equi_in%time_slice(1)%profiles_2d(ireg)%grid%dim1
  ZZ  =>equi_in%time_slice(1)%profiles_2d(ireg)%grid%dim2
  Rbnd=>equi_in%time_slice(1)%boundary%lcfs%r
  Zbnd=>equi_in%time_slice(1)%boundary%lcfs%z

  Raxis= equi_in%time_slice(1)%global_quantities%magnetic_axis%r
  Zaxis= equi_in%time_slice(1)%global_quantities%magnetic_axis%z

  phi_edge = equi_in%time_slice(1)%profiles_1d%phi(size(equi_in%time_slice(1)%profiles_1d%phi))
  phi_rz   = equi_in%time_slice(1)%profiles_2d(ireg)%phi

  flux_rz_norm = phi_rz / phi_edge

  flux_norm_out(1)=0
  flux_norm_out(nfluxout)=1
  do i=1,nfluxout-2
    flux_norm_out(i+1) = dble(i)*1.0/dble(nfluxout-1)
  enddo
  ! Now one has an equidistant mesh with nfluxout points between 0 and 1
  if (krho_option .ge. 10) then
    ! equidistant on rho_tor means Phi should be square of the equidistant mesh
    flux_norm_out(1:nfluxout) = flux_norm_out(1:nfluxout)*flux_norm_out(1:nfluxout)
  end if
  thetamesh(1)=0
  thetamesh(ntheta_out)=dble(2)*PI

  do i=1,ntheta_out-2
    thetamesh(i+1) = dble(i)*dble(2)*PI/dble(ntheta_out-1)
  enddo


  farray_in(:,:,1) = flux_rz_norm
  farray_in(:,:,2) = equi_in%time_slice(1)%profiles_2d(ireg)%b_r
  farray_in(:,:,3) = equi_in%time_slice(1)%profiles_2d(ireg)%b_z
  farray_in(:,:,4) = equi_in%time_slice(1)%profiles_2d(ireg)%b_tor

  rhobnd=sqrt((Rbnd-Raxis)**2.0 + (Zbnd-Zaxis)**2.0)
  thetabnd=atan2(Zbnd-Zaxis,Rbnd-Raxis)

  do i=1,sizeRbnd
    if (thetabnd(i)<0) then
      thetabnd(i) = thetabnd(i) + 2.0*PI
    endif
  enddo
  if (abs(thetabnd(sizeRbnd)-thetabnd(1)) .lt. 1e-6) then
    sizeRbndtmp    = sizeRbnd - 1
    allocate(thetabndtmp(sizeRbndtmp),rhobndtmp(sizeRbndtmp))
    allocate(Rbndtmp(sizeRbndtmp))
    do i=1,sizeRbndtmp
      thetabndtmp(i) = thetabnd(i)
      rhobndtmp(i)   = rhobnd(i)
      Rbndtmp(i)     = Rbnd(i)
    enddo
  else
    sizeRbndtmp = sizeRbnd
    allocate(thetabndtmp(sizeRbndtmp),rhobndtmp(sizeRbndtmp))
    allocate(Rbndtmp(sizeRbndtmp))
    thetabndtmp = thetabnd
    rhobndtmp   = rhobnd
    Rbndtmp     = Rbnd
  endif

  !sorting result by theta
  do j=2,sizeRbndtmp
    aux1=thetabndtmp(j)
    aux2=rhobndtmp(j)
    do i=j-1,1,-1
      if (thetabndtmp(i)<=aux1) goto 10
      thetabndtmp(i+1)=thetabndtmp(i)
      rhobndtmp(i+1)=rhobndtmp(i)
    end do
    i=0
10  thetabndtmp(i+1)=aux1
    rhobndtmp(i+1)=aux2
  enddo

  call interpos(thetabndtmp,rhobndtmp,nin=size(Rbndtmp),&
    nout=ntheta_out,tension=-0.1,xout=thetamesh,yout=rhobound_thetamesh, nbc=-1,ybc=2.*PI)

  ! Create a polar rho mesh as fraction of rhobound for each thetamesh
  sigma(1)=0.0
  sigma(nsigma)=1.0
  do i=1,nsigma-2
    sigma(i+1) = dble(i)*1.0/dble(nsigma-1)
  enddo

  do i=1,ntheta_out
    rhomesh(:,i) = sigma*rhobound_thetamesh(i)
  enddo

  ! Compute flux_norm on (rho,theta) mesh by interpolating psi(R,Z) on these Rrho,Rtheta points
  do j=1,ntheta_out
    do i=1,nsigma
      thetamesh2D(i,j)=thetamesh(j)
    enddo
  enddo
  Rrhotheta = Raxis + rhomesh*cos(thetamesh2D)
  Zrhotheta = Zaxis + rhomesh*sin(thetamesh2D)
  call cos_interpos2d_cartesian(RR,ZZ,farray_in,Rrhotheta,Zrhotheta,farray_out,tension_default)
  ! make sure of edge values of normalized flux
  farray_out(1,:,1)        = 0.
  farray_out(nfluxout,:,1) = 1.

  ! compute rho polar for each theta corresponding to desired flux surfaces, now that flux_norm known on rho,theta
  ! use sqrt(flux) since flux not good for inverse interpolation near axis
  do j=1,ntheta_out
    call interpos(sqrt(farray_out(1:nfluxout,j,1)),rhomesh(1:nfluxout,j),nin=nfluxout,nout=nfluxout,tension=tension_default,&
      xout=sqrt(flux_norm_out),yout=rhopolar_at_flux(:,j), &
      nbc=(/2, 2/),ybc=(/rhomesh(1,j), rhomesh(nfluxout,j) /))
    farray_out_fluxnormtheta(:,j,1) = flux_norm_out
    do k=2,size(farray_out,3)
      call interpos(rhomesh(1:nfluxout,j),farray_out(1:nfluxout,j,k),&
        nin=nfluxout,nout=nfluxout,tension=tension_default,&
        xout=rhopolar_at_flux(:,j),yout=farray_out_fluxnormtheta(:,j,k), &
        nbc=(/2, 2/),ybc=(/farray_out(1,j,k), farray_out(nfluxout,j,k)/))
    end do
  end do

  ! Impose magnetic axis values which are known:
  farray_out_fluxnormtheta(1,:,2) = 0._r8; ! b_r = 0 on axis
  farray_out_fluxnormtheta(1,:,3) = 0._r8; ! b_z = 0 on axis
  farray_out_fluxnormtheta(1,:,4) = equi_in%time_slice(1)%global_quantities%magnetic_axis%b_tor; ! b_tot = b_mag on axis

  Rrhotheta_atflux = Raxis + rhopolar_at_flux*cos(thetamesh2D)
  Zrhotheta_atflux = Zaxis + rhopolar_at_flux*sin(thetamesh2D)

  do j=1,ntheta_out
    do i=1,nfluxout
      thetamesh2Dnew(i,j) = thetamesh(j)
    enddo
  enddo

  if (flagadd==1) then
    iprofile=size(equi_out%time_slice(1)%profiles_2d)
  else
    iprofile=ireg
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%grid%dim1)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%grid%dim2)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%b_r)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%b_z)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%b_tor)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%r)
    deallocate(equi_out%time_slice(1)%profiles_2d(ireg)%z)
  endif

  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim1(nfluxout))
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim2(ntheta_out))
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%b_r(nfluxout,ntheta_out)) 
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%b_z(nfluxout,ntheta_out))
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%b_tor(nfluxout,ntheta_out))
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%r(nfluxout,ntheta_out))
  allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%z(nfluxout,ntheta_out))

  if (mod(krho_option,10) .eq. 0) then
    ! to get rho_tor_norm in dim1
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim1= sqrt(farray_out_fluxnormtheta(:,1,1))
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%index=ids_inverse_rhotor_norm_polar
    allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%name(1))
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%name(1) = 'flux_rhotornorm_polar'
    allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1))
    if (krho_option .eq. 0) equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1) = &
      'flux surface type with radial label is sqrt[Phi/Phi_edge], Phi being toroidal flux, theta as polar angle, equidistant Phi'
    if (krho_option .eq. 10) equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1) = &
      'flux surface type with radial label is sqrt[Phi/Phi_edge], Phi being toroidal flux, theta as polar angle, equidistant rho_tor_norm'
  else if (mod(krho_option,10) .eq. 1) then
    ! to get rho_tor in dim1
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim1= sqrt(farray_out_fluxnormtheta(:,1,1) * phi_edge/B0/PI)
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%index=ids_inverse_rhotor_polar
    allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%name(1))
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%name(1) = 'flux_rhotor_polar'
    allocate(equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1))
    if (krho_option .eq. 1) equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1) = &
      'flux surface type with radial label is sqrt[Phi/B0/pi], Phi being toroidal flux, theta as polar angle, equidistant Phi'
    if (krho_option .eq. 11) equi_out%time_slice(1)%profiles_2d(iprofile)%grid_type%description(1) = &
      'flux surface type with radial label is sqrt[Phi/B0/pi], Phi being toroidal flux, theta as polar angle, equidistant rho_tor'
  else
    write(0,*) 'krho_option = ',krho_option, &
      & ' not defined in cos_rz2rhotheta_calc, ask O. Sauter. Meahwhile Phi put in dim1'
    ! to get Phi_tor in dim1
    equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim1= farray_out_fluxnormtheta(:,1,1) * phi_edge
  end if
  equi_out%time_slice(1)%profiles_2d(iprofile)%grid%dim2= thetamesh2Dnew(1,:) 
  equi_out%time_slice(1)%profiles_2d(iprofile)%b_r      = farray_out_fluxnormtheta(:,:,2)
  equi_out%time_slice(1)%profiles_2d(iprofile)%b_z      = farray_out_fluxnormtheta(:,:,3)
  equi_out%time_slice(1)%profiles_2d(iprofile)%b_tor    = farray_out_fluxnormtheta(:,:,4)
  equi_out%time_slice(1)%profiles_2d(iprofile)%r        = Rrhotheta_atflux
  equi_out%time_slice(1)%profiles_2d(iprofile)%z        = Zrhotheta_atflux
end subroutine cos_rz2rhotheta_calc

subroutine cos_interpos2d_cartesian(RR,ZZ,f2darray_in,Rout,Zout,f2darray_out,tension_in,kextrapol)
  !
  ! subroutine interpos2d_cartesian(RR,ZZ,f2darray_in,Rout,Zout,f2darray_out[,tension_in,kextrapol])
  !
  ! interpolate f(RR,ZZ) onto Rout,Zout assuming 1D meshes RR(:) and ZZ(:)
  ! f is each of k=1,nin from f2darray_in(:,:,k)
  ! Rout, Zout can be 2d series of points of a 1d chord. 
  ! To match assumed 2d dimension, the "1d" Rout case should be given with size(Rout,2)=1 and same for Zout
  !
  ! Inputs:
  !   RR(:):            R mesh (1D) of f2darray_in
  !   ZZ(:):            Z mesh (1D) of f2darray_in
  !   f2darray_in(:,:,1:nin): quantities to interpolate on the points (Rout,Zout)
  !   Rout(:,:):        R values on which the quantities are requested
  !   Zout(:,:):        Z values on which the quantities are requested
  !   tension_in (optional): (default=-0.1 if not given) tension used for interpolation. Can use -0.01 for less smoothing, -1.,-3. or -10 for more smoothing
  !   kextrapol (optional): extrapolation option (see below)
  !
  ! Outputs:
  !   f2darray_out(:,:,1:nin): values of f2darray_in interpolated on (Rout,Zout) points
  !
  ! extrapolation is performed according to kextrapol (same for R and Z at this stage):  
  !     kextrapol = 0 => constant outside the (R_in,Z_in) domain with f_extrapol = f_edge
  !     kextrapol = 1 => linear extrapolation with continuous derivative along the direction (either R or Z)
  !     kextrapol = 2 => quadratic extrapolation with continuous derivative
  !     kextrapol = 3 (default)=> cubic extrapolation nearby and then quadratic
  !     kextrapol = 4 => cubic extrapolation (only safe if does not extrapolate very far)
  !     kextrapol = 5 => no extrapolation. Get a warning if it does extrapolate
  !
  use interpos_module
  !
  IMPLICIT NONE
  !
  real*8, intent(IN) :: RR(:), ZZ(:), f2darray_in(:,:,:), Rout(:,:), Zout(:,:)
  real*8, intent(IN), optional :: tension_in
  integer, intent(IN), optional :: kextrapol
  !
  real*8, allocatable :: f2darray_out(:,:,:)
  !
  integer :: k, jin, iout, jout, kextrapol_eff, nverbose, nR_in, nZ_in, ndims_f2darray_in(3), nin, &
    & nRZ_out_dim1, nRZ_out_dim2, iopt_interpos, nbc(2)
  real*8 :: tension_eff, tension_R, tension_Z, ybc(2)
  real*8, allocatable :: f2d_Rout_Zin(:,:)
  !
  nverbose = 3
  !
  tension_eff = -0.1_8
  if (present(tension_in)) then
    tension_eff = tension_in
  end if
  !
  kextrapol_eff = 3
  if (present(kextrapol)) then
    kextrapol_eff = kextrapol
  end if
  ! check sizes
  nR_in = size(RR)
  nZ_in = size(ZZ)
  ndims_f2darray_in = shape(f2darray_in)
  if (ndims_f2darray_in(1) .NE. nR_in) then
    write(6,*) 'size(f2darray_in,1) should be equal to size(RR)'
    call flush(6)
    return
  end if
  if (ndims_f2darray_in(2) .NE. nZ_in) then
    write(6,*) 'size(f2darray_in,2) should be equal to size(ZZ)'
    call flush(6)
    return
  end if
  nin = ndims_f2darray_in(3)
  nRZ_out_dim1 = size(Rout,1)
  nRZ_out_dim2 = size(Rout,2)
  if ((size(Zout,1) .NE. nRZ_out_dim1) .OR. (size(Zout,2) .NE. nRZ_out_dim2)) then
    write(6,*) 'shape(Zout) should be equal to shape(Rout)'
    call flush(6)
    return
  end if
  if ((size(f2darray_out,1) .NE. nRZ_out_dim1) .OR. (size(f2darray_out,2) .NE. nRZ_out_dim2) .OR. (size(f2darray_out,3) .NE. nin)) then
    write(6,*) 'shape(f2darray_out) should be equal to (size(Rout,1),size(Rout,2),nin)'
    call flush(6)
    return
  end if
  !
  select case (kextrapol_eff)
  case(0)
    iopt_interpos = 63; ! constant f_extrapo = f_edge
  case(1)
    iopt_interpos = 23; ! linear with continuous derivative
  case(2)
    iopt_interpos = 33; ! quadratic with continuous derivative
  case(3)
    iopt_interpos = 13; ! cubic nearby then quadratic (default interpos)
  case(4)
    iopt_interpos = 43; ! cubic 
  case(5)
    iopt_interpos = 3; ! no extrapolation, get warning from interpos if extrapolating
  case default
    write(6,*) 'case kextrapol = ',kextrapol,' not yet implmented, check kextrapol in interpos2d_cartesian'
    call flush(6)
    return
  end select
  !
  allocate(f2d_Rout_Zin(nRZ_out_dim1,nZ_in))
  do k=1,nin
    tension_R = tension_eff
    tension_Z = tension_eff
    nbc = (/0, 0/) ! to allow tension = 0, one should put (0, 0)
    ybc = (/0._8, 0._8 /)
    do jout=1,nRZ_out_dim2
      ! loop over j and first spline on 1st dimension
      do jin=1,nZ_in
        call interpos(RR,f2darray_in(:,jin,k),nR_in,nout=nRZ_out_dim1,tension=tension_eff,xout=Rout(:,jout),yout=f2d_Rout_Zin(:,jin),nbc=nbc,ybc=ybc,option=iopt_interpos)
      end do
      ! then interpolate on Z using f2d_Rout_Zin(Rout(1:nRZ_out_dim1,jout),ZZ(1:nZ_in))
      do iout=1,nRZ_out_dim1
        call interpos(ZZ,f2d_Rout_Zin(iout,:),nZ_in,xscal=Zout(iout,jout),tension=tension_eff,yscal=f2darray_out(iout,jout,k),nbcscal=nbc,ybcscal=ybc,option=iopt_interpos)
      end do
    end do
  end do
  !
  deallocate(f2d_Rout_Zin)
  return
  !
end subroutine cos_interpos2d_cartesian

